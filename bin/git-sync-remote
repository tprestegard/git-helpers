#! /usr/bin/env bash

#########
# Setup #
#########
set -euo pipefail

# Default values
DEFAULT_REMOTE=upstream
REMOTE_BRANCH=$(git symbolic-ref refs/remotes/origin/HEAD | sed 's@^refs/remotes/origin/@@')

# Basename of this script
NAME=$(basename "${0}")

# Import helpers
. $( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/.helpers/all

#######################
# Help/usage messages #
#######################
USAGE="Usage:
  ${NAME} [-b | --remote-branch BRANCH-NAME] [REMOTE_NAME]
  ${NAME} -h | --help"

HELP="${NAME} - sync primary branch of current repo with branch in remote repo.
  Fails if there are uncommitted changes on current branch or if unable to do a
  fast-forward merge. Uses 'upstream' as the default remote if REMOTE_NAME is
  not provided.

${USAGE}

Options:
  -b, --remote-branch=BRANCH    Name of branch in remote repo to merge into
                                primary branch of current repo.
                                [default: ${REMOTE_BRANCH}]
  -h, --help                    Print this message and exit.

Examples:
  # Sync with '${REMOTE_BRANCH}' branch on '${DEFAULT_REMOTE}' remote
  ${NAME}

  # Sync with 'feature1' branch on 'other-remote' remote
  ${NAME} --remote-branch=feature1 other-remote"

###############
# Parse flags #
###############
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
    case $1 in
        -b|--remote-branch|--remote-branch=*)
            REMOTE_BRANCH=$(parse_flag "${1}" "${2-''}")
            [[ "${1}" == *"="* ]] || shift
            shift
            ;;

        -h|--help)
            echo "${HELP}"
            exit 0
            ;;

        -*|--*)
            echo "Unknown option $1."
            echo -e "\n${USAGE}"
            exit 1
            ;;

        *)
            POSITIONAL_ARGS+=("$1") # save positional arg
            shift
            ;;

    esac
done

# Restore positional parameters
set -- "${POSITIONAL_ARGS[@]}"

# Validate and assign positional parameters
if [[ "$#" -gt 1 ]]; then
    echo "${USAGE}"
    exit 1
fi
REMOTE_NAME="${1-${DEFAULT_REMOTE}}"

#############
# Main code #
#############
# Get primary branch name of this repo
PRIMARY_BRANCH=$(basename $(git symbolic-ref "refs/remotes/origin/HEAD"))

# Check if any uncommitted changes - untracked changes should be OK
if ! $(git diff-index --quiet HEAD); then
    echo "Error: Uncommitted changes in repo; resolve these, then retry."
    exit 1
fi

# Get current branch
CURRENT_BRANCH=$(git branch --show-current)

# Run code
git checkout "${PRIMARY_BRANCH}" && \
    git fetch "${REMOTE_NAME}" && \
    git lfs fetch --all "${REMOTE_NAME}" && \
    git merge --ff-only "${REMOTE_NAME}/${REMOTE_BRANCH}"

# Return to original branch
git checkout "${CURRENT_BRANCH}"
