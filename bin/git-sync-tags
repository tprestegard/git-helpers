#! /usr/bin/env bash

#########
# Setup #
#########
set -euo pipefail

# Default values
SOURCE_REMOTE=${1-upstream}
RECEIVER_REMOTE=${2-origin}

# Basename of this script
NAME=$(basename "${0}")

# Import helpers
. $( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/.helpers/all

#######################
# Help/usage messages #
#######################
USAGE="Usage:
  ${NAME} [REMOTE_TAG_SOURCE] [REMOTE_TAG_[-b | --remote-branch BRANCH-NAME] [REMOTE_NAME]
  ${NAME} -h | --help"

HELP="${NAME} - sync tags from one remote to another. Typically used to sync
  tags from an upstream project into a personal fork. Uses 'upstream' as the
  tag source and 'origin' as the tag receiver by default.

${USAGE}

Options:
  -h, --help                    Print this message and exit.

Examples:
  # Sync tags from 'upstream' remote to 'origin' remote
  ${NAME}

  # Sync tags from 'r1' remote to 'r2' remote.
  ${NAME} r1 r2"

###############
# Parse flags #
###############
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
    case $1 in
        -h|--help)
            echo "${HELP}"
            exit 0
            ;;

        -*|--*)
            echo "Unknown option $1."
            echo -e "\n${USAGE}"
            exit 1
            ;;

        *)
            POSITIONAL_ARGS+=("$1") # save positional arg
            shift
            ;;

    esac
done

# Restore positional parameters
set -- "${POSITIONAL_ARGS[@]}"

# Validate and assign positional parameters
if [[ "$#" -gt 2 ]]; then
    echo "${USAGE}"
    exit 1
fi

#############
# Main code #
#############
git tag -l | xargs git tag -d
git fetch --tags "${SOURCE_REMOTE}"
git push --tags "${RECEIVER_REMOTE}"
